package com.yousuf.image.downloader;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Yousuf Syed
 */
public class DownloaderUtils {

    /**
     * Check if device is connected to internet.
     *
     * @param ctx app context
     * @return true if connected, false otherwise.
     */
    public static boolean isConnectedToNetwork(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

    /**
     * Convert InputStream Response into String
     *
     * @param is input stream response
     * @return Response string.
     * @throws IOException
     */
    public static String getStringFromStream(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String nextLine = "";

        while ((nextLine = reader.readLine()) != null) {
            sb.append(nextLine);
        }
        return sb.toString();
    }


    public static void CopyStream(InputStream is, OutputStream os, int length) throws IOException {
        byte[] bytes = new byte[1024];
        int count;
        int delta;
        int contentLength = 0;
        while ((count = is.read(bytes, 0, 1024)) != -1 && (delta = length - contentLength) > 0) {
            count = (count > delta)? delta : count; //if content length is different that actual count.
            os.write(bytes, 0, count); //Write byte from output stream
            contentLength += count;
        }
    }

}
