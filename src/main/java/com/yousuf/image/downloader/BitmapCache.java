package com.yousuf.image.downloader;

import android.graphics.Bitmap;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Yousuf Syed
 */
public class BitmapCache {

    private static final String TAG = "BitmapCache";

    private Map<String, Bitmap> cache;

    private long limit = 1000000;

    private long size = 0;

    public BitmapCache() {
        cache = Collections.synchronizedMap(new LinkedHashMap<String, Bitmap>(10, 1.5f, true)); //Last argument true for LRU ordering
        limit = Runtime.getRuntime().maxMemory() / 4;  // 25% of the total heap.
    }

    public Bitmap getBitmap(String key) {
        Bitmap bitmap = null;
        if (cache.containsKey(key)) {
            bitmap = cache.get(key);
        }
        return bitmap;
    }

    public void addBitmap(String key, Bitmap bitmap) {
        if (!cache.containsKey(key)) {
            cache.put(key, bitmap);
            size += getBytes(bitmap);
            checkSize();
        }
    }

    public boolean contains(String key){
        return cache.containsKey(key);
    }

    private void checkSize() {
        //least recently accessed item will be the first one iterated & removed if size greater than limit.
        Iterator<Map.Entry<String, Bitmap>> iterator = cache.entrySet().iterator();
        while (iterator.hasNext() && (size > limit)) {
            Map.Entry<String, Bitmap> entry = iterator.next();
            size -= getBytes(entry.getValue());
            iterator.remove();
        }
    }

    private long getBytes(Bitmap bmp) {
        if (bmp != null) {
            return bmp.getRowBytes() * bmp.getHeight();
        }
        return 0;
    }

    public void clear() {
        if (cache != null) {
            cache.clear();
        }
    }
}
