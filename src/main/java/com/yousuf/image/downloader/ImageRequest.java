package com.yousuf.image.downloader;

import android.content.Context;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by u471637 on 3/8/16.
 */
public class ImageRequest {

    private Context mContext;

    private WeakReference<ImageView> mImageReference;

    private String mUrlString;

    private boolean isScaled;

    private ImageRequest(ImageRequestBuilder builder) {
        setContext(builder.mContext);
        setImageReference(builder.mImageReference);
        setUrlString(builder.mUrlString);
        setIsScaled(builder.isScaled);
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public WeakReference<ImageView> getImageReference() {
        return mImageReference;
    }

    public void setImageReference(WeakReference<ImageView> mImageReference) {
        this.mImageReference = mImageReference;
    }

    public String getUrlString() {
        return mUrlString;
    }

    public void setUrlString(String mUrlString) {
        this.mUrlString = mUrlString;
    }

    public boolean isScaled() {
        return isScaled;
    }

    public void setIsScaled(boolean isScaled) {
        this.isScaled = isScaled;
    }

    public static class ImageRequestBuilder {

        private Context mContext;

        private String mUrlString;

        private boolean isScaled;

        private WeakReference<ImageView> mImageReference;

        public ImageRequestBuilder(Context ctx ) {
            mContext = ctx;
        }

        public ImageRequestBuilder setImage(ImageView image) {
            mImageReference = new WeakReference(image);
            return this;
        }

        public ImageRequestBuilder setUrl(String url) {
            mUrlString = url;
            return this;
        }

        public ImageRequestBuilder setScaleDown(boolean scale) {
            isScaled = scale;
            return this;
        }

        public ImageRequest build(){
            return new ImageRequest(this);
        }
    }
}
