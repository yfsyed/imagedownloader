package com.yousuf.image.downloader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Yousuf Syed
 */
public class RestClientUtils {

    /**
     * Get feeds
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static String getContent(String url) throws IOException {
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            int statusCode = urlConnection.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                return DownloaderUtils.getStringFromStream(urlConnection.getInputStream());
            }
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

    /**
     * @param url
     * @return
     * @throws IOException
     */
    public static Bitmap getBitmap(String url) throws Exception {
        InputStream is;
        Bitmap bmp = null;
        HttpURLConnection urlConnection = null;
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();
                if (is != null) {
                    bmp = decodeFile(is);
                    is.close();
                }
            }
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return bmp;
    }


    public static Bitmap getFeatureBitmap(String url) throws Exception {
        InputStream is;
        Bitmap bmp = null;
        HttpURLConnection urlConnection = null;
        Log.i("REST CLIENT", url);
        try {
            URL uri = new URL(url);
            urlConnection = (HttpURLConnection) uri.openConnection();
            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                is = urlConnection.getInputStream();
                if (is != null) {
                    bmp = BitmapFactory.decodeStream(is);
                    is.close();
                }
            }
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
        return bmp;
    }

    public static void saveBitmapToFile(final File file, final Bitmap bmp) throws IOException {
        if (file == null || bmp == null) {
            return;
        }

        FileOutputStream fos = new FileOutputStream(file);
        bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.flush();
        fos.close();
    }

    /**
     * Decode and save file to internal storage.
     *
     * @param file file
     * @return
     */
    public static Bitmap decodeFile(File file) throws Exception {
        FileInputStream fis = new FileInputStream(file);
        Bitmap bitmap = BitmapFactory.decodeStream(fis);
        fis.close();
        return bitmap;
    }

    public static Bitmap decodeFile(InputStream is) throws Exception {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = false;
        o.inSampleSize = 8;
        Bitmap bitmap = BitmapFactory.decodeStream(is, null, o);
        is.close();
        return bitmap;
    }

}
