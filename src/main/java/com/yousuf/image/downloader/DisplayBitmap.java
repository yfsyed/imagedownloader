package com.yousuf.image.downloader;

import android.graphics.Bitmap;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by ysyed2 on 2/27/16.
 */
public class DisplayBitmap implements Runnable {
    WeakReference<ImageView> imageRef;
    Bitmap bitmap;

    public DisplayBitmap(WeakReference imageReference, Bitmap bmp) {
        imageRef = imageReference;
        bitmap = bmp;
    }

    @Override
    public void run() {
        ImageView thumbnail = imageRef.get();
        if (thumbnail != null && bitmap != null) {
            thumbnail.setImageBitmap(bitmap);
        }
    }
}
