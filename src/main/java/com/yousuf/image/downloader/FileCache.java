package com.yousuf.image.downloader;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by u471637 on 2/15/16.
 */
public class FileCache {

    private File mCacheDir;

    public FileCache(Context ctx) throws FileNotFoundException {
        // mCacheDir = new File(ctx.getFilesDir(), "/data/RssFeeds");

        mCacheDir = getFeedsStorageDir(ctx, "/ImageDownloads");
        if (mCacheDir == null) {
            mCacheDir = new File(ctx.getFilesDir(), "/data/ImageDownloads");
        }
        if (!mCacheDir.exists())
            mCacheDir.mkdirs();

    }

    public File getFile(String url) {
        String fileName = String.valueOf(url.hashCode()) + ".png";
        return new File(mCacheDir, fileName);
    }

    public void clear() {
        (new Runnable() {

            @Override
            public void run() {
                File[] files = mCacheDir.listFiles();
                if (files != null) {
                    for (File f : files) {
                        if (f.exists()) {
                            f.delete();
                        }
                    }
                }
            }
        }).run();

    }

    // Get the directory for the app's private pictures directory.
    public File getFeedsStorageDir(Context context, String FeedsName) {
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), FeedsName);
        if (!file.mkdirs()) {
            Log.e("File Cache", "Feeds Directory not created");
        }
        return file;
    }
}
