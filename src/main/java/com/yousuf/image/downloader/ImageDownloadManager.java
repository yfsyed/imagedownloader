package com.yousuf.image.downloader;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Yousuf Syed.
 */
public class ImageDownloadManager {

    private static ImageDownloadManager sInstance; // Singleton instance of the Bitmap Loader that provides cached bitmap or fetches one from the url if

    public final String LAST_CACHE_INTERVAL = "last-cache-timestamp";

    private long DEFAULT_CACHE_INTERVAL = 1 * 60 * 60 * 1000;

    private Map<ImageView, String> mImageViewMap;

    private ExecutorService mExecutorService;

    private ExecutorService mFileExecutorService;

    private BitmapCache mBitmapCache;

    private FileCache mFileCache;

    private Handler mHandler;

    private ImageDownloadManager() {

    }

    /**
     * Initialize the cache and executor instances.
     * Generally, init from Application instance.
     *
     * @param ctx
     */
    public void init(Context ctx) {
        // make sure that we init only once.
        if (mImageViewMap == null) {
            mImageViewMap = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
            mExecutorService = Executors.newFixedThreadPool(4);
            mFileExecutorService = Executors.newFixedThreadPool(4);
            mBitmapCache = new BitmapCache();
            mHandler = new Handler();

            try {
                mFileCache = new FileCache(ctx);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Singleton instance to the Download Manager
     *
     * @return instance of DownloadManager
     */
    public static ImageDownloadManager getInstance() {
        if (sInstance == null) {
            sInstance = new ImageDownloadManager();
        }
        return sInstance;
    }

    /**
     * Set cache policy, default is 24 hours.
     *
     * @param millis time interval in milliseconds.
     */
    public void setCache(long millis) {
        DEFAULT_CACHE_INTERVAL = millis;
    }

    /**
     * Load ImageView with either a cached bitmap
     * or download from network if no cache is available.
     *
     * @param thumbnail image view on which bitmap is loaded.
     */
    public void loadBitmap(final ImageView thumbnail, final String imageUrl, boolean scaled) {
        if (thumbnail != null && !TextUtils.isEmpty(imageUrl)) {
            mImageViewMap.put(thumbnail, imageUrl);
            Bitmap bitmap = mBitmapCache.getBitmap(imageUrl);
            if (bitmap != null) {
                thumbnail.setImageBitmap(bitmap);
            } else {
                mExecutorService.execute(new BitmapLoader(thumbnail, imageUrl, scaled));
                Log.v("BITMAP_LOADER", "adding to executor");
            }
        }
        Log.v("BITMAP_LOADER", "imageUrl: " + imageUrl);
    }

    /**
     * Get an individual image from the url.
     *
     * @param url url from where the image is to be fetched.
     * @return downloaded bitmap.
     */
    public Bitmap getFeatureBitmap(String url) {
        Bitmap bmp = null;

        if (mFileCache != null) {
            File f = mFileCache.getFile(url);
            try {
                bmp = RestClientUtils.decodeFile(f);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (bmp == null) {
                Log.v("BITMAP_LOADER", "running http call to get bitmap");
                try {
                    bmp = RestClientUtils.getFeatureBitmap(url);
                    mFileExecutorService.submit(new BitmapSaver(f, bmp));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return bmp;
    }

    /**
     * Check whether the image view was reused to display another image.
     *
     * @param thumbnailReference weak reference to the thumbnail
     * @param url
     * @return
     */
    private boolean isImageViewReused(WeakReference<ImageView> thumbnailReference, String url) {
        if (thumbnailReference != null && !TextUtils.isEmpty(url)) {
            ImageView thumbnail = thumbnailReference.get();
            if (thumbnail != null) {
                String tag = mImageViewMap.get(thumbnail);
                if (tag == null || !tag.equals(url)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Clear cache when exiting the activity.
     * All though the clear happens if the cache expired which is currently set to 1 hour.
     * You can change the Cache interval through setCache(int milles)
     *
     * @param ctx
     */
    public void clearCache(Context ctx) {
        if (isCacheExpired(ctx)) {

            if (mBitmapCache != null) {
                mBitmapCache.clear();
            }

            if (mImageViewMap != null) {
                mImageViewMap.clear();
            }

            if (mFileCache != null) {
                mFileCache.clear();
            }
        }
    }

    /**
     * @param ctx
     * @return
     */
    private boolean isCacheExpired(Context ctx) {
        SharedPreferences preferences = ctx.getSharedPreferences("com.sample.image.downloader", Context.MODE_PRIVATE);
        long current = System.currentTimeMillis();
        long lastCache = preferences.getLong(LAST_CACHE_INTERVAL, 0);

        if (current - lastCache > DEFAULT_CACHE_INTERVAL) {
            preferences.edit().putLong(LAST_CACHE_INTERVAL, current).apply();
            return true;
        }
        return false;
    }

    /**
     * Runnable that fetches the bitmap from internal storage or from network.
     */
    private class BitmapLoader implements Runnable {
        WeakReference<ImageView> imageReference;
        String url;
        boolean scaled;

        BitmapLoader(ImageView imgView, String imgUrl, boolean scaled) {
            url = imgUrl;
            this.scaled = scaled;
            imageReference = new WeakReference(imgView);
        }

        @Override
        public void run() {
            Bitmap bmp = null;

            if (mFileCache != null) {
                File f = mFileCache.getFile(url);
                try {
                    bmp = RestClientUtils.decodeFile(f);
                } catch (Exception e) {
                    // e.printStackTrace();
                }

                if (bmp == null) {
                    Log.v("BITMAP_LOADER", "running http call to get bitmap");
                    try {
                        if (scaled) {
                            bmp = RestClientUtils.getBitmap(url);
                        } else {
                            bmp = RestClientUtils.getFeatureBitmap(url);
                        }
                        mFileExecutorService.submit(new BitmapSaver(f, bmp));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                mBitmapCache.addBitmap(url, bmp);

                if (isImageViewReused(imageReference, url)) {
                    return;
                }
                mHandler.post(new DisplayBitmap(imageReference, bmp));
            }
        }
    }


    /**
     * Get an individual image from the url
     *
     * @param imageUrl url from where the image is to be fetched.
     * @param listener callback where the bitmap will be sent once its downloaded.
     */
    public void getBitmapFromUrl(String imageUrl, BitmapResponseListener listener) {
        if (!TextUtils.isEmpty(imageUrl)) {
            Bitmap bitmap = mBitmapCache.getBitmap(imageUrl);
            if (bitmap != null) {
                if (listener != null) {
                    listener.onBitmapReceived(bitmap);
                }
            } else {
                mExecutorService.execute(new BitmapPicker(listener, imageUrl));
                Log.v("BITMAP_LOADER", "adding to executor");
            }
        }
        Log.v("BITMAP_LOADER", "imageUrl: " + imageUrl);
    }


    /**
     * Runnable send to executor for processing
     */
    private class BitmapPicker implements Runnable {
        String url;
        BitmapResponseListener mListener;

        BitmapPicker(BitmapResponseListener listener, String imgUrl) {
            url = imgUrl;
            mListener = listener;
        }

        @Override
        public void run() {
            Bitmap bmp = null;

            if (mFileCache != null) {
                File f = mFileCache.getFile(url);
                try {
                    bmp = RestClientUtils.decodeFile(f);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (bmp == null) {
                    Log.v("BITMAP_LOADER", "running http call to get bitmap");
                    try {
                        bmp = RestClientUtils.getFeatureBitmap(url);
                        mFileExecutorService.submit(new BitmapSaver(f, bmp));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                mBitmapCache.addBitmap(url, bmp);
                if (mListener != null) {
                    mListener.onBitmapReceived(bmp);
                }
            }
        }
    }
}
