package com.yousuf.image.downloader;

import android.graphics.Bitmap;

import java.io.File;

/**
 * Created by ysyed2 on 2/27/16.
 */
public class BitmapSaver implements Runnable {

    File file;
    Bitmap bmp;

    public BitmapSaver(final File f, final Bitmap bitmap) {
        file = f;
        bmp = bitmap;
    }

    @Override
    public void run() {
        try {
            RestClientUtils.saveBitmapToFile(file, bmp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

