# README #

### What is this repository for? ###

* Quick summary:
  This library is for lazy loading of images, or fetching bitmaps synchronously.

* Current Version : 1.0.1

### How do I get set up? ###


#### Library Setup: ####

  Add the following one time initialization to the Application object or the launcher Activity:
  ImageDownloadManager.getInstance().init(getApplicationContext());

  * To initiate Asynchronous image download:

   
```
#!java

 ImageDownloadManager.getInstance().loadBitmap(
           mImageView, // imageview to set the image
           imageUrl, // image url 
           false    // if true, bitmap is scaled down by factor of 8. if false displays the as is version. 
    );
```


  * To get bitmap synchronously:

    
```
#!java

Bitmap bitmap = ImageDownloadManager.getInstance().getFeatureBitmap(imageUrl);
```


#### Dependencies ####

  * Add the following to build.gradle files:

    
```
#!java

repositories {
        maven {
              url  "http://dl.bintray.com/ysyed/maven" 
        }
    }
```


  * Add the following to the dependencies:

      
```
#!java

 compile 'com.yousuf.imagedownloader:BitmapDownlaoder:1.0.1'
```

### Issues ### 

If you encounter errors with NavigationView use buildtoolsversion >= 23.0.2

```
#!java

buildToolsVersion '23.0.2'
```


### Who do I talk to? ###
Please let me know if you have any questions or comments regarding this library

* myname.yousuf@gmail.com